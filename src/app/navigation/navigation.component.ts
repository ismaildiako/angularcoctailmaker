import { Component, OnInit ,HostListener} from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  public innerWidth: any;

  @HostListener('window:resize', ['$event'])onResize(event:any) {
    this.innerWidth = window.innerWidth;
    console.log(window.innerWidth)
  }
  constructor() { }

  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
  }


}
