export interface CocktailInterface {
  id: number;
  coctailName: string;
  description: string;
  imgPath: string,
  cocKtail_Ingrediants: IngredientInterface[];
}

export interface IngredientInterface {
  id: number;
  name: string;
  amount_cl: number;
}

export interface SendCocktailInterface {
  id: number;
  cocktailName: string;
  description: string;
  imgPath: string,
}

export interface sendIngredientInterface {
  Id: number
  IngrideientName: string,
  Amount_cl: number
}
export interface sendIngredientInterfaceTwo {
  id: number
  name: string,
  amount_cl: number
}
export interface Amount {
  value: string;
  viewValue: string;
}

