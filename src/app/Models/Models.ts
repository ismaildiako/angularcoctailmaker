import {sendIngredientInterface} from "../Interface/Interfases";

export class SendCocktail {


  constructor(CoctailName: string, Description: string, Name: string, ImgPath: string) {
    this.CoctailName = CoctailName;
    this.Description = Description;
    this.Name = Name;
    this.ImgPath = ImgPath;
  }

  CoctailName!: string;
  Description!: string;
  Name!: string;
  ImgPath!: string;
}

export class SendIngredient {
  constructor(Id: number | undefined, IngrideientName: string, Amount_cl: number) {
    this.Id = Id;
    this.IngrideientName = IngrideientName;
    this.Amount_cl = Amount_cl;
  }

  Id!: number | undefined;
  IngrideientName!: string;
  Amount_cl!: number;
}

export class NameClass {
  name!: string


}
