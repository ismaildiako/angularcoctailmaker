import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {HttpClient, HttpEventType} from "@angular/common/http";
import {ApiService} from "../api.service";
import {FormControl, FormGroup, NgForm} from "@angular/forms";


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  constructor(private CocktailService: ApiService, private http: HttpClient) {}
  progress!: string;
  imgObj!: any;
  cocktail_id!:number;


  cocktailForm = new FormGroup({
    name: new FormControl(''),
    desc: new FormControl(''),
  })

  @Output() public onUploadFinished = new EventEmitter();

  ngOnInit(): void {}

  createCocktail() {
    console.log(this.CocktailService.getUserNameService());
    this.CocktailService.addCocktail(this.cocktailForm.value.name, this.cocktailForm.value.desc, this.imgObj.dbPath.toString())
      .subscribe(data => {
        console.log(data)
      this.cocktail_id = data.id;
      })
  }


  uploadFile = (files: any) => {

    if (files.length === 0) {
      return;
    }
    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);


    this.http.post('https://localhost:7045/api/Upload', formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          this.progress = "Upload failed"
        else if (event.type === HttpEventType.Response) {

          this.onUploadFinished.emit(event.body);
          this.imgObj = event.body;

          if (event.status== 200){
            this.createCocktail()
          }
        }
      });

  }

  public createImgPath = (serverPath: string) => {
    return "https://localhost:7045/" + serverPath;
  }
}
