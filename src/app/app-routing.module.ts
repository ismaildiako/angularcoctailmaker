import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CocktailListComponent} from "./cocktail-list/cocktail-list.component";
import {CardComponent} from "./card/card.component";
import {HomeComponent} from "./home/home.component";
import {SignoutComponent} from "./signout/signout.component";


const routes: Routes = [

  { path: '', component: HomeComponent },
  { path: 'list', component: CocktailListComponent },
  { path: 'create', component: CardComponent },
  { path: 'logout', component: SignoutComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
