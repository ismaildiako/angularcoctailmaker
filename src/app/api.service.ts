import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable} from 'rxjs';
import {
  CocktailInterface,
  IngredientInterface,
  SendCocktailInterface,
  sendIngredientInterface, sendIngredientInterfaceTwo
} from "./Interface/Interfases";
import {SendCocktail, SendIngredient} from "./Models/Models";
import {FormControl} from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiServiceUrl = 'https://localhost:7045'

  constructor(private http: HttpClient) {
  }

  public getCocktails(): Observable<CocktailInterface[]> {
    return this.http.get<CocktailInterface[]>(`${this.apiServiceUrl}/api/Cocktail`)
  }

  public getIngredients(): Observable<IngredientInterface[]> {
    return this.http.get<IngredientInterface[]>(`${this.apiServiceUrl}/api/Ingredient`)
  }

  public getIngredientNames(): Observable<string[]> {
    return this.http.get<string[]>(`${this.apiServiceUrl}/api/Ingredient/ingredient_names`)
  }

  public addCocktail(cocktail_name: string, description: string, filePath: string): Observable<SendCocktailInterface> {
    console.log(this._userNameService);
    console.log(cocktail_name);
    console.log(filePath);
    console.log(description);


    const cocktailObj = new SendCocktail(cocktail_name, description, this._userNameService, filePath);
    return this.http.post<SendCocktailInterface>(`${this.apiServiceUrl}/api/Cocktail/cocktailModel`, cocktailObj)
  }

  public addIngredientToCocktail(id: number | undefined, ingrideientName: string, amount_cl: number): Observable<sendIngredientInterfaceTwo> {
    const ingredientObj = new SendIngredient(id, ingrideientName, amount_cl);
    console.log("cocktail id", id);
    console.log(ingrideientName);
    console.log(amount_cl);
    return this.http.post<sendIngredientInterfaceTwo>(`${this.apiServiceUrl}/api/Ingredient/add_ingrediant`, ingredientObj);
  }

  public deleteIngredientFromCocktail(cocktail_id: number, ingredient_id: number): Observable<any> {
    let queryParams = new HttpParams();
    queryParams = queryParams.append("id", cocktail_id);
    queryParams = queryParams.append("ingredientId", ingredient_id);
    return this.http.delete(`${this.apiServiceUrl}/api/Cocktail/ingredeient_from_coctail`, {params: queryParams});
  }

  public createUser(username: string): Observable<any> {
    let requestName = {username}
    return this.http.post(`${this.apiServiceUrl}/api/User/postuser`, requestName);
  }

  public doseUserExsist(username: string): Observable<any> {
    return this.http.get<any>(`${this.apiServiceUrl}/api/User/${username}`);
  }

  public deleteCocktail(cocktail_id: number): Observable<any> {
    return this.http.delete(`${this.apiServiceUrl}/api/Cocktail/${cocktail_id}`);
  }

  public getUserById(): Observable<any> {
    let params = new HttpParams().set("id", this._userId);
    return this.http.get<any>(`${this.apiServiceUrl}/api/User/getuserbyid`, {params});
  }

  public getUserByName(): Observable<any> {
    let val:string =JSON.stringify( sessionStorage.getItem('user'));
   let str = val.slice(1, -1);
    let params = new HttpParams().set("name",str);
    return this.http.get<any>(`${this.apiServiceUrl}/api/User/getuserbyname`, {params});
  }

  private _userNameService!: string;

  getUserNameService(): string {
    return this._userNameService;
  }

  setUserNameService(value: string) {
    this._userNameService = value;
  }

  private _userId!: number;

  getUserId(): number {
    return this._userId;
  }

  setUserId(value: number) {
    this._userId = value;
  }
}
