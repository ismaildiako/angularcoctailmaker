import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import {catchError, Observable, retry} from 'rxjs';
import {ApiService} from "../api.service";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor() {
  }

  auth_token =  sessionStorage.getItem('jwt') ;



  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const tokenizedReq = request.clone({
      setHeaders:{
        'Const-Type':'application/json',
        Authorization:`Bearer ${this.auth_token}`
      }
    })
    return next.handle(tokenizedReq).pipe(
      retry(1)
    );
  }
}
