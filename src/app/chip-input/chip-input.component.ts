import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {map, Observable, startWith} from "rxjs";
import {MatChipInputEvent} from "@angular/material/chips";
import {MatAutocompleteSelectedEvent} from "@angular/material/autocomplete";
import {ApiService} from "../api.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Amount, sendIngredientInterfaceTwo} from "../Interface/Interfases";

@Component({
  selector: 'app-chip-input',
  templateUrl: './chip-input.component.html',
  styleUrls: ['./chip-input.component.css']
})
export class ChipInputComponent implements OnInit {
  @Input() cocktail_id_Parent!: number ;
  amountForm= new FormGroup({
    amount: new FormControl('0')
  })

  ingredientsObjects: sendIngredientInterfaceTwo[] = [];
  ingredientList!: string[];
  separatorKeysCodes: number[] = [ENTER, COMMA];
  ingredientCtrl = new FormControl();
  filteredIngredients!: Observable<string[]>;
  ingredients: string[] = [];


  @ViewChild('ingredientInput') textInput!: ElementRef<HTMLInputElement>;


  onlyNr(event:any):boolean{

    const charCode =(event.which)? event.which: event.keyCode;

    if (charCode > 35 &&  (charCode < 48 || charCode >57)){
      console.log('charCode resttrickded is ' + charCode)
      return false;
    }
    return true;
  }

  constructor(private IngredientService: ApiService) {
    this.filteredIngredients = this.ingredientCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: string | null) => (fruit ? this._filter(fruit) : this.ingredientList)),
    );
  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    console.log('hej hej')
    this.addIngredient()
    if (value) {
      this.ingredients.push(value);
    }
    // Clear the input value
    event.chipInput!.clear();
    this.ingredientCtrl.setValue(null);
  }

  remove(item: any): void {
    const index = this.ingredients.indexOf(item);
    this.deleteIngredient(this.cocktail_id_Parent, item.id)
    if (index >= 0) {
      this.ingredients.splice(index, 1);
    }


    const removeIndex= this.ingredientsObjects.findIndex(i=>i.name === item.name);
  this.ingredientsObjects.splice(removeIndex,1)

  }

  deleteIngredient(cocktail_id: number, ingredient_id: number) {
    this.IngredientService.deleteIngredientFromCocktail(cocktail_id, ingredient_id).subscribe(item => {
      console.log(item);
    })
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.ingredients.push(event.option.viewValue);
    this.textInput.nativeElement.value = '';
    this.ingredientCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.ingredientList.filter(item => item.toLowerCase().includes(filterValue));
  }

  public getIngredientNameList(): void {
    this.IngredientService.getIngredientNames().subscribe(
      (response: string[]) => {
        this.ingredientList = response;
        console.log(this.ingredientList)
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  addIngredient() {
    this.IngredientService.addIngredientToCocktail(this.cocktail_id_Parent, this.ingredientCtrl.value, this.amountForm.value.amount)
      .subscribe(item => {
        this.ingredientList.push(item.name);
        // console.log(item, this.ingredientList)
      console.log(item)
        this.ingredientsObjects.push(item);
        console.log("Kalles " + this.ingredientsObjects)
      })

  }

  ngOnInit(): void {
    this.getIngredientNameList();
  }


}
