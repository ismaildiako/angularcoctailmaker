import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CocktailListComponent} from "./cocktail-list/cocktail-list.component";
import {ChipInputComponent} from "./chip-input/chip-input.component";
import {CardComponent} from "./card/card.component";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {MaterialModule} from "./material/material.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NavigationComponent} from "./navigation/navigation.component";
import {UploadComponent} from './upload/upload.component';
import {HomeComponent} from './home/home.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {AmplifyAuthenticatorModule} from "@aws-amplify/ui-angular";
import {TokenInterceptor} from "./services/token.interceptor";
import { SignoutComponent } from './signout/signout.component';


@NgModule({
  declarations: [
    AppComponent,
    CocktailListComponent,
    ChipInputComponent,
    NavigationComponent,
    CardComponent,
    UploadComponent,
    HomeComponent,
    SignoutComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    AmplifyAuthenticatorModule


  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
