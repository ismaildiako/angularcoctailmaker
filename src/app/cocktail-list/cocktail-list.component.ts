import { Component, OnInit } from '@angular/core';
import {CocktailInterface} from "../Interface/Interfases";
import {ApiService} from "../api.service";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-cocktail-list',
  templateUrl: './cocktail-list.component.html',
  styleUrls: ['./cocktail-list.component.css']
})
export class CocktailListComponent implements OnInit {
    public cocktails!:CocktailInterface[];
  constructor(private CocktailService:ApiService) { }

  ngOnInit(): void {
   this.getCocktailList();
    this.CocktailService.getUserByName().subscribe(item =>
   {
     console.log('User from back end ',item);
     this.cocktails =item.cocktails;
     sessionStorage.setItem("userId",item.id)

     const obj = sessionStorage.getItem("userId")
     console.log(obj);


   })
  }

  public getCocktailList():void{
    this.CocktailService.getCocktails().subscribe(
      (response:CocktailInterface[])=>{
        this.cocktails = response;
        // console.log(this.cocktails)
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }
  public createImgPath = (serverPath: string) => {
    return "https://localhost:7045/"+serverPath;
  }

  deleteIngredient(cocktail_id: number, ingredient_id: number) {
    this.CocktailService.deleteIngredientFromCocktail(cocktail_id, ingredient_id).subscribe(item => {
      console.log(item);
      this.getCocktailList()
    })
  }
  deleteCocktail(cocktail_id: number) {
    this.CocktailService.deleteCocktail(cocktail_id).subscribe(item => {
      console.log(item);
      this.getCocktailList()
    })
  }
}
