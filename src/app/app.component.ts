import {Component, ChangeDetectorRef, Output} from '@angular/core';
import {onAuthUIStateChange, CognitoUserInterface, AuthState} from '@aws-amplify/ui-components';
import {Hub} from 'aws-amplify';
import {ApiService} from "./api.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CocktailMakerClient';
  user: CognitoUserInterface | undefined;
  authState!: AuthState;
  userName!: '';
  emailVerified = false;


  constructor(private ref: ChangeDetectorRef, private CocktailService: ApiService) {}

  ngOnInit() {
    Hub.listen('auth', (data) => {
      if (data.payload.event=='signIn') {
        this.userName = data.payload.data.username;
        this.emailVerified = data.payload.data.attributes.email_verified;
        this.CocktailService.setUserNameService(this.userName)


        sessionStorage.setItem('jwt',data.payload.data.signInUserSession.accessToken.jwtToken);
        console.log('Looking for tokan :', data.payload.data.signInUserSession.accessToken.jwtToken)

      }

      console.log(this.userName);
      console.log(this.emailVerified);




      if (this.emailVerified == true ) {
        this.CocktailService.doseUserExsist(this.userName).subscribe(item =>{
          if (item !== true ){
            this.createUserInDb(this.userName)
          }
          console.log(" boolian :", item)
        });
      }






      switch (data.payload.event) {
        case 'signIn':
          sessionStorage.setItem('user',data.payload.data.username.toString());
          console.log('user',data.payload.data.username);
          console.log('user session', sessionStorage.getItem('user'));
          console.log('user signed in ', this.user?.username, data);
          break;
        case 'signUp':
          sessionStorage.setItem('user',data.payload.data.username.toString());
          console.log('user signed up ', this.user?.username, data);
          break;
        case 'signOut':
          console.log('user signed out ', this.user?.username, data);
          break;
        case 'signIn_failure':
          console.log('user sign in failed ', this.user?.username, data);
          break;
        case 'configured':
          console.log('the Auth module is configured ', this.user?.username, data);
      }
    });

    onAuthUIStateChange((authState, authData) => {
      this.authState = authState;
      this.user = authData as CognitoUserInterface;
      this.ref.detectChanges();
    })


  }

  createUserInDb(name:string) {
    this.CocktailService.createUser(name).subscribe(item => {

      console.log("User created", item);
    })
  }


  ngOnDestroy() {
    return onAuthUIStateChange;
  }
}
